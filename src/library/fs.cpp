// fs.cpp: File System

/*
NOTES
1) debug() - determine Data Block number, how to iterate thru indirect pointers, indirect Data Block number
2) format() - verify write superblock... how to "clear blocks"
3) mount() - how to "set device and mount", what is the "metadata" to copy, and block bitmap (member variable)
*/

#include "sfs/fs.h"

#include <algorithm>
#include <iostream>

#include <assert.h>
#include <stdio.h>
#include <string.h>

// Debug file system -----------------------------------------------------------

void FileSystem::debug(Disk *disk) {
    Block block;

    // Read Superblock
    disk->read(0, block.Data);

    printf("SuperBlock:\n");
    if (block.Super.MagicNumber == MAGIC_NUMBER)
        printf("    magic number is valid\n");
    else
        printf("    magic number is not valid\n");
    printf("    %u blocks\n"         , block.Super.Blocks);
    printf("    %u inode blocks\n"   , block.Super.InodeBlocks);
    printf("    %u inodes\n"         , block.Super.Inodes);

    // Read Inode blocks
    Block iblock;
    for (uint32_t i = 1; i <= block.Super.InodeBlocks; i++) {  //  Loop through inode blocks.
        disk->read(i, iblock.Data);
        for (uint32_t j = 0; j < INODES_PER_BLOCK; j++) {      //  Loop through inodes in each block.
            Inode inode = iblock.Inodes[j];
            if (inode.Valid) {
                printf("Inode %d:\n", j);
                printf("    size: %u bytes\n", inode.Size);
                printf("    direct blocks:");
                for (uint32_t k = 0; k < POINTERS_PER_INODE; k++) { 
                    if (inode.Direct[k]) {
                        printf(" %lu", (unsigned long)(inode.Direct[k]));
                    }
                }
                printf("\n");
                if (inode.Indirect) {  
                    printf("    indirect block: %lu\n", (unsigned long)(inode.Indirect));
                    // uint32_t n = 0;
                    printf("    indirect data blocks:");
                    Block Indirect_block;
                    disk->read(inode.Indirect, Indirect_block.Data);
                    for (uint32_t l = 0; l < POINTERS_PER_BLOCK; l++) { // Loop throught pointers from indirect block
                        if (Indirect_block.Pointers[l])
                            printf(" %d",(Indirect_block.Pointers[l]));
                    }
                    printf("\n"); 
                } 
            }
        }
    }
}

// Format file system ----------------------------------------------------------

bool FileSystem::format(Disk *disk) {
    // Check if disk is mounted.
    if (disk->mounted())
        return false;

    int block_index = 0;

    // Write superblock
    Block block;
    block.Super.MagicNumber = MAGIC_NUMBER;
    block.Super.Blocks = disk->size();
    block.Super.InodeBlocks = std::ceil(block.Super.Blocks * 0.10);
    block.Super.Inodes = block.Super.InodeBlocks * INODES_PER_BLOCK;
    disk->write(block_index, block.Data);
    block_index++;

    /* Check this by manually running test for outputs ^^^^^^^^^^^^^^^^^^^^^^*/
    //loop through inodeblocks and write to each
    for (uint32_t i = block_index; i <= block.Super.InodeBlocks; i++) {
        //Clear inodes in inode block
        for (uint32_t j = 0; j < INODES_PER_BLOCK; j++) {
            block.Inodes[j].Valid = 0;
            block.Inodes[j].Size = 0;
            for (uint32_t k = 0; k < POINTERS_PER_INODE; k++) {
                block.Inodes[j].Direct[k] = 0;
            }
            block.Inodes[j].Indirect = 0;
        }
        disk->write(block_index, block.Data);
        block_index++;
    }
    //Clear pointers within indirect block
    for (uint32_t i = 0; i < POINTERS_PER_BLOCK; i++) {
        block.Pointers[i] = 0;
    }
    disk->write(block_index, block.Data);
    block_index++;

    //Clear blocks in data block
    for (uint32_t i = 0; i < Disk::BLOCK_SIZE; i++) {
        block.Data[i] = 0;
    }

    //Write to the remaining data blocks
    for (uint32_t i = block_index; i < disk->size(); i++) {
        disk->write(block_index, block.Data);
        block_index++;
    }
     /* Check this by manually running test for outputs ^^^^^^^^^^^^^^^^^^^^^^*/

    return true;
}

// Mount file system -----------------------------------------------------------

bool FileSystem::mount(Disk *disk) {
    // Check if there is no device
    if (Device)
        return false;

    // Check if there is not a disk currently mounted
    if (disk->mounted())
        return false;

    // Read superblock
    Block block;
    disk->read(0, block.Data);

    // Check disk for valid Superblock values
    if (block.Super.MagicNumber != MAGIC_NUMBER)  //  Check MagicNumber
        return false;
    if (block.Super.Blocks != disk->size())  //  Check Blocks
        return false;
    if (block.Super.InodeBlocks != std::ceil(block.Super.Blocks * 0.10))  //  Check 10%
        return false;
    if (block.Super.Inodes != block.Super.InodeBlocks * INODES_PER_BLOCK)  //  Check Inodes
        return false;

    // Set device and mount
    Device = disk;
    Device->mount();

    // Copy metadata 
    super_block = block.Super;

    // Allocate free block bitmap 
    // TODO - pretty sure this is right

    //go through and read the blocks and store the whether it is allocated in free block bitmap
    for (uint32_t i = 1; i <= block.Super.InodeBlocks; i++) {  //  Loop through inode blocks.
        disk->read(i, block.Data);
        for (uint32_t j = 0; j < INODES_PER_BLOCK; j++) {      //  Loop through inodes in each block.
            Inode inode = block.Inodes[j];
            if (inode.Valid) {
                for (uint32_t k = 0; k < POINTERS_PER_INODE; k++) { 
                    if (inode.Direct[k]) {
                        free_block_bitmap.emplace(inode.Direct[k], 1); //1 for allocated
                    }
                }
                if (inode.Indirect) { 
                    free_block_bitmap.emplace(inode.Indirect, 1);
                    Block Indirect_block;
                    disk->read(inode.Indirect, Indirect_block.Data);
                    for (uint32_t l = 0; l < POINTERS_PER_BLOCK; l++) { // Loop throught pointers from indirect block
                        if (Indirect_block.Pointers[l])
                            free_block_bitmap.emplace(Indirect_block.Pointers[l], 1);
                    }
                } 
            }
        }
    }

    //demo show the map values
    // for (std::map<int,int>::iterator it = free_block_bitmap.begin(); it != free_block_bitmap.end(); it++) {
    //     std::cout << "block: " << it->first << " filled: " << it->second << "\n";
    // }
    return true;
}

// Create inode ----------------------------------------------------------------

ssize_t FileSystem::create() {
    // Locate free inode in inode table
    Block block;
    for (uint32_t i = 1; i <= super_block.InodeBlocks; i++) {  //  Loop through inode blocks.
        Device->read(i, block.Data);
        for (uint32_t j = 0; j < INODES_PER_BLOCK; j++) {      //  Loop through inodes in each block.
            if (!block.Inodes[j].Valid) {
                block.Inodes[j].Valid = 1;
                Device->write(i, block.Data);
                return ((i - 1) * INODES_PER_BLOCK) + j; //return the exact block inumber
            }
        }
    }
    return -1; //this return is failed create because we are off by 127 reads less (this is ok)
}

// Remove inode ----------------------------------------------------------------

bool FileSystem::remove(size_t inumber) {
    // Load inode information
    Block block;
    for (uint32_t i = 1; i <= super_block.InodeBlocks; i++) {  //  Loop through inode blocks.
        Device->read(i, block.Data);
        for (uint32_t j = 0; j < INODES_PER_BLOCK; j++) {  //  Loop through inode blocks. 
            Inode inode = block.Inodes[j];
            if (inumber == ((i - 1) * INODES_PER_BLOCK) + j) { //check if exact block number
                if (!block.Inodes[j].Valid) { return false; } // return false if not valid
                block.Inodes[j].Valid = 0;
                Device->write(i, block.Data);
                if (inode.Direct[j]) { // Free direct blocks
                    for (uint32_t k = 0; k < POINTERS_PER_INODE; k++) { 
                        free_block_bitmap.emplace(inode.Direct[k], 0);
                        block.Inodes[j].Direct[k] = 0; // Clear inode in inode table
                    }
                }
                if (inode.Indirect) {
                    free_block_bitmap.emplace(inode.Indirect, 0);
                    Block Indirect_block;
                    Device->read(inode.Indirect, Indirect_block.Data);
                    for (uint32_t l = 0; l < POINTERS_PER_BLOCK; l++) { 
                        if (Indirect_block.Pointers[l]) { // Free indirect blocks
                            free_block_bitmap.emplace(Indirect_block.Pointers[l], 0);
                            Indirect_block.Pointers[l] = 0; // Clear inode in inode table
                        }
                    }
                }
            }
        }
    }
    return true;
}

// Inode stat ------------------------------------------------------------------

ssize_t FileSystem::stat(size_t inumber) {
    Block block;
    for (uint32_t i = 1; i <= super_block.InodeBlocks; i++) {  //  Loop through inode blocks.
        Device->read(i, block.Data);
        for (uint32_t j = 0; j < INODES_PER_BLOCK; j++) {  //  Loop through inode blocks. 
            Inode inode = block.Inodes[j];
            if (inumber == ((i - 1) * INODES_PER_BLOCK) + j) {
                if (block.Inodes[j].Valid)
                    return inode.Size;
                else
                    return -1;
            }
        }
    }
    return -1;
}

// Read from inode -------------------------------------------------------------

ssize_t FileSystem::read(size_t inumber, char *data, size_t length, size_t offset) {
    // Load inode information
    Block block;
    for (uint32_t i = 1; i <= super_block.InodeBlocks; i++) {  //  Loop through inode blocks.
        Device->read(i, block.Data);
        for (uint32_t j = 0; j < INODES_PER_BLOCK; j++) {  //  Loop through inode blocks. 
            Inode inode = block.Inodes[j];
            if (inumber == ((i - 1) * INODES_PER_BLOCK) + j) {
                if (inode.Valid) {
                    // Adjust length
                    uint32_t adjusted_length = std::min((inode.Size - offset), length);

                    // Read block and copy to data
                    Block read_block;
                    size_t index = std::floor(offset / Disk::BLOCK_SIZE);
                    printf("offset: %lu\n", offset);
                    printf("Disk::BLOCK_SIZE: %lu\n", Disk::BLOCK_SIZE);
                    printf("index: %lu\n", index);

                    if (index < POINTERS_PER_INODE) {  //  Direct Blocks
                        Device->read(inode.Direct[index], read_block.Data);
                        memcpy(data, read_block.Data + (offset % Disk::BLOCK_SIZE), adjusted_length);
                        return adjusted_length;
                    } else {                           //  Indirect Blocks
                        if (inode.Indirect) {
                            Block indirect_block;
                            Device->read(inode.Indirect, indirect_block.Data);
                            Device->read(indirect_block.Pointers[index - POINTERS_PER_INODE], read_block.Data);
                            memcpy(data, read_block.Data + (offset % Disk::BLOCK_SIZE), adjusted_length);
                            return adjusted_length;
                        } else {
                            return -1;
                        }
                    }
                } else {
                    return -1;
                }
            } 
        }
    }
    return -1;
}

// Write to inode --------------------------------------------------------------

ssize_t FileSystem::write(size_t inumber, char *data, size_t length, size_t offset) {
    // Load inode
    Block block;
    for (uint32_t i = 1; i <= super_block.InodeBlocks; i++) {  //  Loop through inode blocks.
        Device->read(i, block.Data);
        for (uint32_t j = 0; j < INODES_PER_BLOCK; j++) {  //  Loop through inode blocks. 
            Inode inode = block.Inodes[j];
            if (inumber == ((i - 1) * INODES_PER_BLOCK) + j) {
                if (inode.Valid) {
                    // Adjust length
                    uint32_t adjusted_length = std::min((inode.Size - offset), length);

                    // Write block and copy to data
                    Block write_block;
                    size_t index = std::floor(offset / Disk::BLOCK_SIZE);
                    if (index < POINTERS_PER_INODE) {
                        Device->read(inode.Direct[index], write_block.Data);
                        // memcpy
                    } else {

                    }
                    // memcpy(inode.Direct, data, adjusted_length);
                    // Device->write(i, &inode.Direct);

                    return adjusted_length;  // Return n bytes
                } else {
                    return -1;
                }
            }
        }
    }
    return -1;
}
